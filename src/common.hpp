/*
 *
 * libcoreaws2/src/common.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__COMMON_HPP
#define COREAWS__COMMON_HPP

#include <ctime>
#include <string>

#include "coreaws/coreaws.hpp"

std::string requestToCurl(coreaws::SignerHandle signer, coreaws::AWSCredentialsHandle credentials, coreaws::RequestHandle request, std::time_t time);
std::string requestToCurl(coreaws::SignerHandle signer, coreaws::AWSCredentialsHandle credentials, coreaws::RequestHandle request);
std::string requestToCurl(coreaws::RequestHandle request);
std::string getInput(coreaws::RequestHandle request);
coreaws::AWSCredentialsHandle readCredentials();

#endif // not COREAWS__COMMON_HPP

