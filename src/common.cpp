/*
 *
 * libcoreaws2/src/common.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "common.hpp"

#include <cstdlib>
#include <fstream>
#include <istream>
#include <sstream>
#include <stdexcept>

#include <iostream>

std::string requestToCurl(coreaws::SignerHandle signer, coreaws::AWSCredentialsHandle credentials, coreaws::RequestHandle request, std::time_t time)
{
    coreaws::RequestHandle signedRequest = (*signer)(credentials, request, time);
    return requestToCurl(signedRequest);
}


std::string requestToCurl(coreaws::SignerHandle signer, coreaws::AWSCredentialsHandle credentials, coreaws::RequestHandle request)
{
    coreaws::RequestHandle signedRequest = (*signer)(credentials, request, coreaws::DateTimeUtils::now());
    return requestToCurl(signedRequest);
}

std::string requestToCurl(coreaws::RequestHandle request)
{
    std::stringstream strbuf;

    strbuf << "curl -v -L \"" << request->url() << '"';

    std::string method = request->method;
    if (method == "POST")
    {
        strbuf << " --request POST";

        std::string queryString = request->queryString();
        if (queryString.length() > 0)
        {
            strbuf << " --data \"" << queryString << '"';
        }

        std::string str = getInput(request);
        if (str.size() > 0)
        {
            strbuf << " --data-binary \"" << str << '"';
        }
    }
    else if (method == "PUT")
    {
        strbuf << " --request PUT";
        strbuf << " --data-binary \"" << getInput(request) << '"';
    }
    else if (method == "DELETE")
    {
        strbuf << " --request DELETE";
    }
    else if (method == "HEAD")
    {
        strbuf << " --head";
    }
    else if (method == "GET")
    {
        strbuf << " --get";
    }
    else
    {
        std::stringstream errbuf;
        errbuf << "Unknown HTTP method: \"" << method << '"';
        throw std::invalid_argument(errbuf.str().c_str());
    }

    coreaws::HeaderMap::const_iterator ptr = request->headers.begin();
    coreaws::HeaderMap::const_iterator end = request->headers.end();
    for (; ptr != end; ++ptr)
    {
        strbuf << " --header \"" << ptr->first;

        if (ptr->second != "")
        {
            strbuf << ": " << ptr->second;
        }
        else if (coreaws::StringUtils::startsWithIgnoreCase(ptr->first, "x-"))
        {
            strbuf << ';';
        }
        else
        {
            strbuf << ':';
        }
        strbuf << '"';
    }

    return strbuf.str();
}

std::string getInput(std::tr1::shared_ptr<coreaws::Request> request)
{
    coreaws::InputHandle inputStream = request->inputStream;
    std::stringstream buf;
    while (inputStream->good())
    {
        char c = inputStream->get();
        if (inputStream->good())
        {
            if (c == '"')
            {
                buf << "\\\"";
            }
            else if (c == '\\')
            {
                buf << "\\\\";
            }
            else
            {
                buf << c;
            }
        }
    }

    return buf.str();
}

coreaws::AWSCredentialsHandle readCredentials()
{
    char* awsCredentialFile = getenv("AWS_CREDENTIAL_FILE");
    std::string path;
    if (NULL != awsCredentialFile)
    {
        path = awsCredentialFile;
    }
    else
    {
        path = "./aws_credentials";
    }

    std::ifstream in(path.c_str());
    if ((in.rdstate() & std::ifstream::failbit) != 0)
    {
        throw std::runtime_error("Cannot open credentials file");
    }

    std::string str;
    in >> str;
    int pos = str.find_first_of("=");
    if (pos == std::string::npos || pos == str.length() - 1)
    {
        throw std::runtime_error("No \"=\" on AWSAccessKeyId line, or no value");
    }

    std::string name = str.substr(0, pos);
    if (name != "AWSAccessKeyId")
    {
        throw std::runtime_error("Variable was not AWSAccessKeyId");
    }

    std::string awsAccessKeyId = str.substr(pos + 1);

    in >> str;
    pos = str.find_first_of("=");

    if (pos == std::string::npos || pos == str.length() - 1)
    {
        throw std::runtime_error("No \"=\" on AWSSecretKey line, or no value");
    }

    name = str.substr(0, pos);
    if (name != "AWSSecretKey")
    {
        throw std::runtime_error("Variable was not AWSSecretKey");
    }

    std::string awsSecretKey = str.substr(pos + 1);

    in >> str;
    pos = str.find_first_of("=");
    std::string sessionToken = "";
    if (pos != std::string::npos && pos != str.length() - 1)
    {
        name = str.substr(0, pos);
        if (name == "SessionToken")
        {
            sessionToken = str.substr(pos + 1);
            coreaws::AWSCredentialsHandle credentials(new coreaws::BasicSessionCredentials(awsAccessKeyId, awsSecretKey, sessionToken));
            return credentials;
        }
    }

    if (sessionToken != "")
    {
        coreaws::AWSCredentialsHandle credentials(new coreaws::BasicSessionCredentials(awsAccessKeyId, awsSecretKey, sessionToken));
        return credentials;
    }
    else
    {
        coreaws::AWSCredentialsHandle credentials(new coreaws::BasicAWSCredentials(awsAccessKeyId, awsSecretKey));
        return credentials;
    }

}

