/*
 *
 * libcoreaws2/src/coreaws/CloudFrontSigner.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "CloudFrontSigner.hpp"

#include <sstream>

#include "DateTimeUtils.hpp"
#include "HeaderUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws
{

RequestHandle CloudFrontSigner::sign(AWSCredentialsHandle credentials,
                                    RequestHandle request,
                                    time_t time)
{
    HeaderMap headers(HeaderUtils::filterAuthorization(request->headers));
    HeaderMap lowerHeaders(HeaderUtils::lowerKey(headers));

    if (lowerHeaders.find("date") == lowerHeaders.end() && lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "Date";
        std::string value = DateTimeUtils::rfc822(time);
        headers.insert(HeaderPair(key, value));
        lowerHeaders.insert(HeaderPair(StringUtils::toLowerCase(key), value));
    }

    std::string stringToSign = CloudFrontSigner::getStringToSign(lowerHeaders);
    std::string signature = Signer::sign("HmacSHA1", credentials->getAWSSecretKey(), stringToSign);
    std::stringstream authorization;
    authorization << "AWS " << credentials->getAWSAccessKeyId() << ':' << signature;
    
    headers.insert(HeaderPair("Authorization", authorization.str()));

    RequestHandle signedRequest(new Request(
            request->method,
            request->scheme,
            request->host,
            request->port,
            request->path,
            request->parameters,
            request->fragment,
            headers,
            request->inputStream));

    return signedRequest;
}

CloudFrontSigner::CloudFrontSigner()
{
}

CloudFrontSigner::~CloudFrontSigner()
{
}

RequestHandle CloudFrontSigner::operator ()(AWSCredentialsHandle credentials,
                                           RequestHandle request,
                                           time_t time) const
{
    return CloudFrontSigner::sign(credentials, request, time);
}

std::string CloudFrontSigner::getStringToSign(const HeaderMap& lowerHeaders)
{
    HeaderMap::const_iterator ptr;

    ptr = lowerHeaders.find("x-amz-date");
    if (ptr != lowerHeaders.end())
    {
        return ptr->second;
    }

    ptr = lowerHeaders.find("date");
    if (ptr != lowerHeaders.end())
    {
        return ptr->second;
    }

    return std::string();
}

}

