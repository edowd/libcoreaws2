/*
 *
 * libcoreaws2/src/coreaws/coreaws.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS_HPP
#define COREAWS_HPP

#include "AWS2Signer.hpp"
#include "AWS3HttpsSigner.hpp"
#include "AWS3Signer.hpp"
#include "AWS4Signer.hpp"
#include "AWSCredentials.hpp"
#include "AWSCredentialsHandle.hpp"
#include "AwsHostNameUtils.hpp"
#include "BasicAWSCredentials.hpp"
#include "BasicSessionCredentials.hpp"
#include "Buffer.hpp"
#include "BufferUtils.hpp"
#include "CloudFrontSigner.hpp"
#include "Connection.hpp"
#include "CryptoUtils.hpp"
#include "DateTimeUtils.hpp"
#include "HeaderMap.hpp"
#include "HeaderPair.hpp"
#include "HeaderUtils.hpp"
#include "InputHandle.hpp"
#include "Method.hpp"
#include "MethodUtils.hpp"
#include "OutputHandle.hpp"
#include "ParameterMap.hpp"
#include "ParameterPair.hpp"
#include "ParameterUtils.hpp"
#include "PathUtils.hpp"
#include "Port.hpp"
#include "Request.hpp"
#include "RequestHandle.hpp"
#include "Response.hpp"
#include "ResponseHandle.hpp"
#include "S3QueryStringSigner.hpp"
#include "S3Signer.hpp"
#include "Scheme.hpp"
#include "SchemeUtils.hpp"
#include "Signer.hpp"
#include "SignerHandle.hpp"
#include "StreamUtils.hpp"
#include "StringUtils.hpp"
#include "UrlUtils.hpp"

#endif // not COREAWS_HPP

