/*
 *
 * libcoreaws2/src/coreaws/AWS4Signer.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "AWS4Signer.hpp"

#include <numeric>
#include <sstream>

#include "AwsHostNameUtils.hpp"
#include "BufferUtils.hpp"
#include "CryptoUtils.hpp"
#include "DateTimeUtils.hpp"
#include "HeaderUtils.hpp"
#include "MethodUtils.hpp"
#include "ParameterUtils.hpp"
#include "PathUtils.hpp"
#include "StreamUtils.hpp"
#include "StringUtils.hpp"

#include <iostream>

namespace coreaws
{


RequestHandle AWS4Signer::sign(AWSCredentialsHandle credentials,
                               RequestHandle request,
                               std::time_t time)
{
    return AWS4Signer::sign(credentials,
                            request,
                            time,
                            AwsHostNameUtils::parseServiceName(request->host),
                            AwsHostNameUtils::parseRegionName(request->host)
                            );
}

RequestHandle AWS4Signer::sign(AWSCredentialsHandle credentials,
                               RequestHandle request,
                               std::time_t time,
                               const std::string& service,
                               const std::string& region)
{
    HeaderMap headers(HeaderUtils::filterAuthorization(request->headers));
    HeaderMap lowerHeaders(HeaderUtils::lowerKey(headers));

    std::string algorithm = "AWS4-HMAC-SHA256";
    std::string terminator = "aws4_request";

    if (lowerHeaders.find("host") == lowerHeaders.end())
    {
        std::string key = "Host";
        std::string value = request->server();
        headers.insert(HeaderPair(key, value));
        lowerHeaders.insert(HeaderPair(StringUtils::toLowerCase(key), value));
    }

    std::string dateTime = DateTimeUtils::iso8601CompressedTimestamp(time);
    std::string dateStamp = DateTimeUtils::iso8601CompressedDate(time);

    if (lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "X-Amz-Date";
        std::string value = dateTime;
        headers.insert(HeaderPair(key, value));
        lowerHeaders.insert(HeaderPair(StringUtils::toLowerCase(key), value));
    }

    std::string canonicalRequest = AWS4Signer::canonicalRequest(request->method, request->path, request->parameters, lowerHeaders, request->inputStream);
//    std::cerr << "CanonicalRequest:" << std::endl << canonicalRequest << std::endl;
    std::string scope = AWS4Signer::scope(dateStamp, region, service, terminator);
    std::string signingCredentials = AWS4Signer::signingCredentials(credentials, scope);
    std::string stringToSign = AWS4Signer::stringToSign(algorithm, dateTime, scope, canonicalRequest);
//    std::cerr << "StringToSign:" << std::endl << stringToSign << std::endl;
    Buffer signature = AWS4Signer::signature(credentials, dateStamp, region, service, terminator, stringToSign);
    std::string authorizationHeader = AWS4Signer::authorizationHeader(algorithm, signingCredentials, AWS4Signer::signedHeadersString(lowerHeaders), signature);
//    std::cerr << "Authorization: " << authorizationHeader << std::endl;

    headers.insert(HeaderPair("Authorization", authorizationHeader));

    RequestHandle signedRequest(new Request(
            request->method,
            request->scheme,
            request->host,
            request->port,
            request->path,
            request->parameters,
            request->fragment,
            headers,
            request->inputStream));

    return signedRequest;
}

std::string AWS4Signer::canonicalRequest(const Method& method,
                                         const std::string& path,
                                         const ParameterMap& parameters,
                                         const HeaderMap& lowerHeaders,
                                         InputHandle inputStream)
{
    std::string canonicalQueryString = ParameterUtils::queryString(parameters);

    std::string queryString;
    if (method != MethodUtils::POST)
    {
        queryString = canonicalQueryString;
    }
    else
    {
        queryString = "";
    }

    std::string input;
    if (method != MethodUtils::POST || canonicalQueryString.empty())
    {
        input = StreamUtils::getInputContents(inputStream);
    }
    else
    {
        input = canonicalQueryString;
    }

    std::stringstream canonicalRequest;
    canonicalRequest << method << '\n';
    canonicalRequest << PathUtils::canonicalPath(path) << '\n';
    canonicalRequest << queryString << '\n';
    canonicalRequest << AWS4Signer::canonicalHeaderString(lowerHeaders) << '\n';
    canonicalRequest << AWS4Signer::signedHeadersString(lowerHeaders) << '\n';
    canonicalRequest << CryptoUtils::toHex(CryptoUtils::hash("sha256", BufferUtils::fromString(input)));

    return canonicalRequest.str();
}

std::string AWS4Signer::canonicalHeaderString(const HeaderMap& lowerHeaders)
{
    return std::accumulate(lowerHeaders.begin(), lowerHeaders.end(), std::string(), AWS4Signer::buildCanonicalHeaderString);
}

std::string AWS4Signer::buildCanonicalHeaderString(const std::string& acc, const HeaderPair& header)
{
    return acc + header.first + std::string(":") + header.second + std::string("\n");
}

std::string AWS4Signer::signedHeadersString(const HeaderMap& lowerHeaders)
{
    std::string signedHeadersString = std::accumulate(lowerHeaders.begin(), lowerHeaders.end(), std::string(), AWS4Signer::buildSignedHeadersString);

    if (!signedHeadersString.empty())
    {
        return signedHeadersString.substr(1);
    }
    else
    {
        return signedHeadersString;
    }
}

std::string AWS4Signer::buildSignedHeadersString(const std::string& acc, const HeaderPair& header)
{
    return acc + std::string(";") + header.first;
}

std::string AWS4Signer::scope(const std::string& dateStamp, const std::string& region, const std::string& service, const std::string& terminator)
{
    std::stringstream strbuf;
    strbuf << dateStamp << '/' << region << '/' << service << '/' << terminator;
    return strbuf.str();
}

std::string AWS4Signer::signingCredentials(AWSCredentialsHandle credentials, const std::string& scope)
{
    return credentials->getAWSAccessKeyId() + std::string("/") + scope;
}

std::string AWS4Signer::stringToSign(const std::string& algorithm,
                                     const std::string& dateTime,
                                     const std::string& scope,
                                     const std::string& canonicalRequest)
{
    std::stringstream stringToSign;
    stringToSign << algorithm << '\n';
    stringToSign << dateTime << '\n';
    stringToSign << scope << '\n';
    stringToSign << CryptoUtils::toHex(CryptoUtils::hash("sha256", BufferUtils::fromString(canonicalRequest)));
    return stringToSign.str();
}

Buffer AWS4Signer::signature(AWSCredentialsHandle credentials,
                             const std::string& dateStamp,
                             const std::string& region,
                             const std::string& service,
                             const std::string& terminator,
                             const std::string& stringToSign)
{
    return CryptoUtils::hmac("sha256", AWS4Signer::kSigning(credentials, dateStamp, region, service, terminator), BufferUtils::fromString(stringToSign));
}

Buffer AWS4Signer::kSigning(AWSCredentialsHandle credentials,
                            const std::string& dateStamp,
                            const std::string& region,
                            const std::string& service,
                            const std::string& terminator)
{
    return CryptoUtils::hmac("sha256", AWS4Signer::kService(credentials, dateStamp, region, service), BufferUtils::fromString(terminator));
}

Buffer AWS4Signer::kService(AWSCredentialsHandle credentials,
                            const std::string& dateStamp,
                            const std::string& region,
                            const std::string& service)
{
    return CryptoUtils::hmac("sha256", AWS4Signer::kRegion(credentials, dateStamp, region), BufferUtils::fromString(service));
}

Buffer AWS4Signer::kRegion(AWSCredentialsHandle credentials,
                           const std::string& dateStamp,
                           const std::string& region)
{
    return CryptoUtils::hmac("sha256", AWS4Signer::kDate(credentials, dateStamp), BufferUtils::fromString(region));
}

Buffer AWS4Signer::kDate(AWSCredentialsHandle credentials,
                         const std::string& dateStamp)
{
    return CryptoUtils::hmac("sha256", AWS4Signer::kSecret(credentials), BufferUtils::fromString(dateStamp));
}

Buffer AWS4Signer::kSecret(AWSCredentialsHandle credentials)
{
    return BufferUtils::fromString(std::string("AWS4") + credentials->getAWSSecretKey());
}

std::string AWS4Signer::authorizationHeader(const std::string& algorithm,
                                            const std::string& signingCredentials,
                                            const std::string& signedHeadersString,
                                            const Buffer& signature)
{
    std::stringstream authorizationHeader;
    authorizationHeader << algorithm << ' ';
    authorizationHeader << "Credential=" << signingCredentials << ", ";
    authorizationHeader << "SignedHeaders=" << signedHeadersString << ", ";
    authorizationHeader << "Signature=" << CryptoUtils::toHex(signature);
    return authorizationHeader.str();
}

AWS4Signer::AWS4Signer() :
    service(""), region("")
{
}

AWS4Signer::AWS4Signer(const std::string& service, const std::string& region) :
    service(service), region(region)
{
}

AWS4Signer::~AWS4Signer()
{
}

RequestHandle AWS4Signer::operator ()(AWSCredentialsHandle credentials, RequestHandle request, std::time_t time) const
{
    if (!service.empty() && !region.empty())
    {
        return AWS4Signer::sign(credentials, request, time, service, region);
    }
    else
    {
        return AWS4Signer::sign(credentials, request, time);
    }
}

}

