/*
 *
 * libcoreaws2/src/coreaws/StreamUtils.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__STREAM_UTILS
#define COREAWS__STREAM_UTILS

#include <cstdlib>
#include <string>

#include "Buffer.hpp"
#include "InputHandle.hpp"
#include "OutputHandle.hpp"

namespace coreaws
{

class StreamUtils
{
public:
    static std::size_t getInputSize(InputHandle inputStream);
    static InputHandle emptyInputStream();
    static OutputHandle emptyOutputStream();
    static InputHandle openInputFile(const std::string& filename);
    static OutputHandle openOutputFile(const std::string& filename);
    static InputHandle setInput(const std::string& input);
    static InputHandle setInput(const Buffer& input);
    static std::string getInputContents(InputHandle inputStream);
};

}

#endif // not COREAWS__STREAM_UTILS

