/*
 *
 * libcoreaws2/src/coreaws/CloudFrontSigner.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__CLOUD_FRONT_SIGNER
#define COREAWS__CLOUD_FRONT_SIGNER

#include "Signer.hpp"

#include <ctime>
#include <string>

#include "HeaderMap.hpp"
#include "HeaderPair.hpp"

namespace coreaws
{

class CloudFrontSigner : public Signer
{
public:
    static RequestHandle sign(AWSCredentialsHandle credentials, RequestHandle request, time_t time);

    CloudFrontSigner();
    virtual ~CloudFrontSigner();
    virtual RequestHandle operator ()(AWSCredentialsHandle credentials, RequestHandle request, time_t time) const;

private:
    static bool isAuthorization(const HeaderPair& pair);
    static std::string getStringToSign(const HeaderMap& lowerHeaders);
};

}

#endif // not COREAWS__CLOUD_FRONT_SIGNER

