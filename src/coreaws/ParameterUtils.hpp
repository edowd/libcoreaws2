/*
 *
 * libcoreaws2/src/coreaws/ParameterUtils.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__PARAMETER_UTILS
#define COREAWS__PARAMETER_UTILS

#include <string>

#include "ParameterMap.hpp"
#include "ParameterPair.hpp"

namespace coreaws
{

class ParameterUtils
{
public:
    static std::string queryString(const ParameterMap& parameters);
    static std::string queryStringNoEmptyEquals(const ParameterMap& parameters);
    static bool isSignature(const ParameterPair& parameter);
    static ParameterMap filterSignature(const ParameterMap& parameters);
private:
    static std::string buildQueryString(const std::string& acc, const ParameterPair& parameter);
    static std::string buildQueryStringNoEmptyEquals(const std::string& acc, const ParameterPair& parameter);
    static std::string checkQueryString(const std::string& queryString);
};

}

#endif // not COREAWS__PARAMETER_UTILS

