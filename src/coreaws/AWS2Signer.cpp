/*
 *
 * libcoreaws2/src/coreaws/AWS2Signer.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "AWS2Signer.hpp"

#include <sstream>
#include <stdexcept>

#include "BufferUtils.hpp"
#include "DateTimeUtils.hpp"
#include "ParameterUtils.hpp"
#include "PathUtils.hpp"
#include "StringUtils.hpp"

#include <iostream>

namespace coreaws
{

RequestHandle AWS2Signer::sign(AWSCredentialsHandle credentials, RequestHandle request, time_t time)
{
    ParameterMap parameters(ParameterUtils::filterSignature(request->parameters));
    
    ParameterMap::const_iterator ptr;
    std::string accessKeyId = credentials->getAWSAccessKeyId();
    std::string key = "AWSAccessKeyId";
    ptr = parameters.find(key);
    if (ptr != parameters.end())
    {
        if (accessKeyId != ptr->second)
        {
            throw std::invalid_argument("AWSAccessKeyId does not match credentials");
        }
    }
    else
    {
        parameters.insert(ParameterPair(key, accessKeyId));
    }

    if (credentials->hasSessionToken())
    {
        std::string sessionToken = credentials->getSessionToken();
        key = "SecurityToken";
        ptr = parameters.find(key);
        if (ptr != parameters.end())
        {
            if (sessionToken != ptr->second)
            {
                throw std::invalid_argument("SecurityToken does not match credentials");
            }
        }
        else
        {
            parameters.insert(ParameterPair(key, sessionToken));
        }
    }

    std::string algorithm;
    key = "SignatureMethod";
    ptr = parameters.find(key);
    if (ptr != parameters.end())
    {
        if (ptr->second != "HmacSHA1" && ptr->second != "HmacSHA256")
        {
            std::stringstream errbuf;
            errbuf << "Unknown SignatureMethod: " << ptr->second;
            throw new std::invalid_argument(errbuf.str());
        }

        algorithm = ptr->second;
    }
    else
    {
        algorithm = "HmacSHA256";
        parameters.insert(ParameterPair(key, algorithm));
    }

    key = "SignatureVersion";
    ptr = parameters.find(key);
    if (ptr != parameters.end())
    {
        if (ptr->second != "2")
        {
            std::stringstream errbuf;
            errbuf << "Unknown SignatureVersion: " << ptr->second;
            throw new std::invalid_argument(errbuf.str());
        }
    }
    else
    {
        parameters.insert(ParameterPair(key, "2"));
    }

    ptr = parameters.find("Timestamp");
    if (ptr == parameters.end())
    {
        ptr = parameters.find("Expires");
        if (ptr == parameters.end())
        {
            parameters.insert(ParameterPair("Timestamp", DateTimeUtils::iso8601(time)));
        }
    }

    Buffer secretAccessKey = BufferUtils::fromString(credentials->getAWSSecretKey());
    std::string stringToSign = AWS2Signer::getStringToSign(request->method, request->server(), request->path, parameters);
    std::string signature = Signer::sign(algorithm, secretAccessKey, stringToSign);

//    std::cerr << "StringToSign = " << stringToSign << std::endl;

    parameters.insert(ParameterPair("Signature", signature));

    RequestHandle signedRequest(new Request(
            request->method,
            request->scheme,
            request->host,
            request->port,
            request->path,
            parameters,
            request->fragment,
            request->headers,
            request->inputStream));

    return signedRequest;
}

std::string AWS2Signer::getStringToSign(
        const Method& method,
        const std::string& server,
        const std::string& path,
        const ParameterMap& parameters)
{
    std::stringstream strbuf;

    strbuf << method << '\n'
           << StringUtils::toLowerCase(server) << '\n'
           << PathUtils::canonicalPath(path) << '\n'
           << ParameterUtils::queryString(parameters);

    return strbuf.str();
}

AWS2Signer::AWS2Signer()
{
}

AWS2Signer::~AWS2Signer()
{
}

RequestHandle AWS2Signer::operator ()(AWSCredentialsHandle credentials, RequestHandle request, std::time_t time) const
{
    return AWS2Signer::sign(credentials, request, time);
}

}

