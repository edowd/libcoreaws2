/*
 *
 * libcoreaws2/src/coreaws/Signer.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__SIGNER
#define COREAWS__SIGNER

#include <ctime>
#include <string>

#include "AWSCredentialsHandle.hpp"
#include "Buffer.hpp"
#include "RequestHandle.hpp"

namespace coreaws
{

class Signer
{
public:
    static std::string sign(const std::string& algorithm, const Buffer& key, const Buffer& data);
    static std::string sign(const std::string& algorithm, const Buffer& key, const std::string& data);
    static std::string sign(const std::string& algorithm, const std::string& key, const Buffer& data);
    static std::string sign(const std::string& algorithm, const std::string& key, const std::string& data);

    Signer();
    virtual ~Signer();
    virtual RequestHandle operator ()(AWSCredentialsHandle credentials, RequestHandle request, time_t time) const = 0;
};

}

#endif // not COREAWS__SIGNER

