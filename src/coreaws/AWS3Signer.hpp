/*
 *
 * libcoreaws2/src/coreaws/AWS3Signer.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__AWS3_SIGNER
#define COREAWS__AWS3_SIGNER

#include "Signer.hpp"

#include <ctime>
#include <string>

#include "HeaderMap.hpp"
#include "HeaderPair.hpp"
#include "InputHandle.hpp"
#include "Method.hpp"
#include "ParameterMap.hpp"

namespace coreaws
{

class AWS3Signer : public Signer
{
public:
    static RequestHandle sign(AWSCredentialsHandle credentials, RequestHandle request, time_t time, const std::string& algorithm);

    AWS3Signer();
    AWS3Signer(const std::string& algorithm);
    virtual ~AWS3Signer();
    virtual RequestHandle operator ()(AWSCredentialsHandle credentials, RequestHandle request, time_t time) const;

    const std::string algorithm;

private:
    static std::string getStringToSign(const Method& method,
                                       const std::string& path,
                                       const ParameterMap& parameters,
                                       const HeaderMap& lowerHeaders,
                                       InputHandle inputStream);
    static std::string getMethodToSign(const Method& method);
    static std::string getPathToSign(const std::string& path);
    static std::string getQueryStringToSign(const ParameterMap& parameters);
    static bool isNotInterestingHeader(const HeaderPair& header);
    static HeaderMap getInterestingHeaders(const HeaderMap& lowerHeaders);
    static std::string getHeadersToSign(const HeaderMap& lowerHeaders);
    static std::string buildHeadersToSign(const std::string& acc, const HeaderPair& header);
    static std::string getSignedHeaders(const HeaderMap& headers);
    static std::string buildSignedHeaders(const std::string& acc, const HeaderPair& header);
};

}

#endif // not COREAWS__AWS3_SIGNER

