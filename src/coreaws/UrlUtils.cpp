/*
 *
 * libcoreaws2/src/coreaws/UrlUtils.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "UrlUtils.hpp"

#include <cstdio>
#include <cstring>
#include <numeric>
#include <sstream>

namespace coreaws
{

std::string UrlUtils::escape(const std::string& str)
{
    return std::accumulate(str.begin(), str.end(), std::string(), UrlUtils::escapeCharacter);
}

std::string UrlUtils::escapePath(const std::string& str)
{
    return std::accumulate(str.begin(), str.end(), std::string(), UrlUtils::escapePathCharacter);
}

std::string UrlUtils::escapeCharacter(const std::string& acc, char c)
{
    char buf[4];
    memset(buf, 0, 4);

    if (UrlUtils::isNotSpecialCharacter(c))
    {
        buf[0] = c;
    }
    else
    {
        buf[0] = '%';
        sprintf(buf + 1, "%02X", c);
    }

    return acc + buf;
}

std::string UrlUtils::escapePathCharacter(const std::string& acc, char c)
{
    char buf[4];
    memset(buf, 0, 4);

    if (UrlUtils::isNotSpecialPathCharacter(c))
    {
        buf[0] = c;
    }
    else
    {
        buf[0] = '%';
        sprintf(buf + 1, "%02X", c);
    }

    return acc + buf;
}

bool UrlUtils::isNotSpecialCharacter(char c)
{
    return (('A' <= c && c <= 'Z') ||
            ('a' <= c && c <= 'z') ||
            ('0' <= c && c <= '9') ||
            c == '-' || c == '_' || c == '.' || c == '~');

}

bool UrlUtils::isNotSpecialPathCharacter(char c)
{
    return (c == '/' || UrlUtils::isNotSpecialCharacter(c));
}

}

