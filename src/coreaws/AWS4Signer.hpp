/*
 *
 * libcoreaws2/src/coreaws/AWS4Signer.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__AWS_4_SIGNER
#define COREAWS__AWS_4_SIGNER

#include "Signer.hpp"

#include "Buffer.hpp"
#include "HeaderMap.hpp"
#include "HeaderPair.hpp"
#include "InputHandle.hpp"
#include "Method.hpp"
#include "ParameterMap.hpp"

namespace coreaws
{

class AWS4Signer : public Signer
{
public:
    static RequestHandle sign(AWSCredentialsHandle credentials,
                              RequestHandle request,
                              std::time_t time);

    static RequestHandle sign(AWSCredentialsHandle credentials,
                              RequestHandle request,
                              std::time_t time,
                              const std::string& service,
                              const std::string& region);

    AWS4Signer();
    AWS4Signer(const std::string& service, const std::string& region);
    ~AWS4Signer();
    virtual RequestHandle operator ()(AWSCredentialsHandle credentials, RequestHandle request, std::time_t time) const;

    const std::string service;
    const std::string region;
private:
    static std::string canonicalRequest(const Method& method,
                                        const std::string& path,
                                        const ParameterMap& parameters,
                                        const HeaderMap& lowerHeaders,
                                        InputHandle inputStream);
    static std::string canonicalHeaderString(const HeaderMap& lowerHeaders);
    static std::string buildCanonicalHeaderString(const std::string& acc, const HeaderPair& header);
    static std::string signedHeadersString(const HeaderMap& lowerHeaders);
    static std::string buildSignedHeadersString(const std::string& acc, const HeaderPair& header);
    static std::string scope(const std::string& dateStamp, const std::string& region, const std::string& service, const std::string& terminator);
    static std::string signingCredentials(AWSCredentialsHandle credentials, const std::string& scope);
    static std::string stringToSign(const std::string& algorithm,
                                    const std::string& dateTime,
                                    const std::string& scope,
                                    const std::string& canonicalRequest);
    static Buffer signature(AWSCredentialsHandle credentials,
                            const std::string& dateStamp,
                            const std::string& region,
                            const std::string& service,
                            const std::string& terminator,
                            const std::string& stringToSign);
    static Buffer kSigning(AWSCredentialsHandle credentials,
                           const std::string& dateStamp,
                           const std::string& region,
                           const std::string& service,
                           const std::string& terminator);
    static Buffer kService(AWSCredentialsHandle credentials,
                           const std::string& dateStamp,
                           const std::string& region,
                           const std::string& service);
    static Buffer kRegion(AWSCredentialsHandle credentials,
                          const std::string& dateStamp,
                          const std::string& region);
    static Buffer kDate(AWSCredentialsHandle credentials,
                        const std::string& dateStamp);
    static Buffer kSecret(AWSCredentialsHandle credentials);
    static std::string authorizationHeader(const std::string& algorithm,
                                           const std::string& signingCredentials,
                                           const std::string& signedHeadersString,
                                           const Buffer& signature);
};

}

#endif // not COREAWS__AWS_4_SIGNER

