/*
 *
 * libcoreaws2/src/coreaws/AWS3HttpsSigner.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__AWS3_HTTPS_SIGNER
#define COREAWS__AWS3_HTTPS_SIGNER

#include "Signer.hpp"

#include <ctime>
#include <string>

#include "HeaderMap.hpp"
#include "HeaderPair.hpp"

namespace coreaws
{

class AWS3HttpsSigner : public Signer
{
public:
    static RequestHandle sign(AWSCredentialsHandle credentials, RequestHandle request, time_t time, const std::string& algorithm);

    AWS3HttpsSigner();
    AWS3HttpsSigner(const std::string& algorithm);
    virtual ~AWS3HttpsSigner();
    virtual RequestHandle operator ()(AWSCredentialsHandle credentials, RequestHandle request, time_t time) const;

    const std::string algorithm;

private:
    static bool isXAmznAuthorization(const HeaderPair& header);
    static std::string getStringToSign(const HeaderMap& lowerHeaders);
};

}

#endif // not COREAWS__AWS3_HTTPS_SIGNER

