/*
 *
 * libcoreaws2/src/coreaws/CryptoUtils.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__CRYPTO_UTILS
#define COREAWS__CRYPTO_UTILS

#include <string>

#include "Buffer.hpp"

namespace coreaws
{

class CryptoUtils
{
public:
    static Buffer hmac(const std::string& digest, const Buffer& key, const Buffer& data);
    static Buffer hash(const std::string& digest, const Buffer& data);
    static std::string encodeBase64(const Buffer& data);
    static Buffer decodeBase64(const std::string& data);
    static std::string toHex(const Buffer& data);
private:
    static std::string buildHex(const std::string& acc, unsigned char c);
};

}

#endif // not COREAWS__CRYPTO_UTILS

