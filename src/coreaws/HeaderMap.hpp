/*
 *
 * libcoreaws2/src/coreaws/HeaderMap.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__HEADER_MAP
#define COREAWS__HEADER_MAP

#include <map>
#include <string>

namespace coreaws
{

typedef std::map<std::string, std::string> HeaderMap;

}

#endif // not COREAWS__HEADER_MAP

