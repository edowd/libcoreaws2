/*
 *
 * libcoreaws2/src/coreaws/BasicSessionCredentials.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "BasicSessionCredentials.hpp"

namespace coreaws
{

BasicSessionCredentials::BasicSessionCredentials(const std::string& awsAccessKeyId, const std::string& awsSecretKey, const std::string& sessionToken)
{
    this->awsAccessKeyId = awsAccessKeyId;
    this->awsSecretKey = awsSecretKey;
    this->sessionToken = sessionToken;
}

BasicSessionCredentials::~BasicSessionCredentials()
{
}

std::string BasicSessionCredentials::getAWSAccessKeyId() const
{
    return this->awsAccessKeyId;
}

std::string BasicSessionCredentials::getAWSSecretKey() const
{
    return this->awsSecretKey;
}

std::string BasicSessionCredentials::getSessionToken() const
{
    return this->sessionToken;
}

bool BasicSessionCredentials::hasSessionToken() const
{
    return (!this->sessionToken.empty());
}

}

