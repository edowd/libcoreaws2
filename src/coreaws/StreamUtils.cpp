/*
 *
 * libcoreaws2/src/coreaws/StreamUtils.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "StreamUtils.hpp"

#include <cstring>
#include <fstream>
#include <sstream>
#include <stdexcept>

#include "BufferUtils.hpp"

namespace coreaws
{

std::size_t StreamUtils::getInputSize(InputHandle inputStream)
{
/*
    inputStream->seekg(0, std::ios::end);
    std::size_t length = inputStream->tellg();
    inputStream->seekg(0, std::ios::beg);

    return length;
*/
    std::streampos cur = inputStream->tellg();
    if (cur == -1)
    {
        return -1;
    }

    inputStream->seekg(0, std::ios::end);
    std::streampos end = inputStream->tellg();
    inputStream->seekg(cur);

    return end - cur;
}

InputHandle StreamUtils::emptyInputStream()
{
    InputHandle inputStream(new std::stringstream());
    return inputStream;
}

OutputHandle StreamUtils::emptyOutputStream()
{
    OutputHandle outputStream(new std::stringstream());
    return outputStream;
}

InputHandle StreamUtils::openInputFile(const std::string& filename)
{
    InputHandle inputStream(new std::ifstream(filename.c_str()));
    if (inputStream->fail())
    {
        std::stringstream errbuf;
        errbuf << "Error opening input file: \"" << filename << '"';
        throw std::runtime_error(errbuf.str());
    }

    return inputStream;
}

OutputHandle StreamUtils::openOutputFile(const std::string& filename)
{
    OutputHandle outputStream(new std::ofstream(filename.c_str()));
//    OutputHandle outputStream(new std::fstream(filename.c_str()));
    if (outputStream->fail())
    {
        std::stringstream errbuf;
        errbuf << "Error opening output file: \"" << filename << '"';
        throw std::runtime_error(errbuf.str());
    }

    return outputStream;
}

InputHandle StreamUtils::setInput(const std::string& input)
{
    InputHandle inputStream(new std::stringstream(input));
    return inputStream;
}

InputHandle StreamUtils::setInput(const Buffer& input)
{
    InputHandle inputStream(new std::stringstream(BufferUtils::toString(input)));
    return inputStream;
}

std::string StreamUtils::getInputContents(InputHandle inputStream)
{
    std::stringstream strbuf;

    inputStream->clear();
    inputStream->seekg(0, std::ios::beg);
    while (inputStream->good())
    {
        char c = inputStream->get();
        if (inputStream->good())
        {
            strbuf << c;
        }
    }
    inputStream->clear();
    inputStream->seekg(0, std::ios::beg);

    return strbuf.str();
}

}

