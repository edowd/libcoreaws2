/*
 *
 * libcoreaws2/src/coreaws/Signer.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "Signer.hpp"

#include <sstream>
#include <stdexcept>

#include "BufferUtils.hpp"
#include "CryptoUtils.hpp"

namespace coreaws
{

std::string Signer::sign(const std::string& algorithm, const Buffer& key, const Buffer& data)
{
    std::string digest;

    if (algorithm == "HmacSHA1")
    {
        digest = "sha1";
    }
    else if (algorithm == "HmacSHA256")
    {
        digest = "sha256";
    }
    else
    {
        std::stringstream errbuf;
        errbuf << "Unknown algorithm: \"" << algorithm << '"';
        throw std::domain_error(errbuf.str());
    }

    return CryptoUtils::encodeBase64(CryptoUtils::hmac(digest, key, data));
}

std::string Signer::sign(const std::string& algorithm, const Buffer& key, const std::string& data)
{
    return Signer::sign(algorithm, key, BufferUtils::fromString(data));
}

std::string Signer::sign(const std::string& algorithm, const std::string& key, const Buffer& data)
{
    return Signer::sign(algorithm, BufferUtils::fromString(key), data);
}

std::string Signer::sign(const std::string& algorithm, const std::string& key, const std::string& data)
{
    return Signer::sign(algorithm, BufferUtils::fromString(key), BufferUtils::fromString(data));
}

Signer::Signer()
{
}

Signer::~Signer()
{
}

}

