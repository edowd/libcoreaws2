/*
 *
 * libcoreaws2/src/coreaws/BufferUtils.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "BufferUtils.hpp"

namespace coreaws
{

Buffer BufferUtils::fromString(const std::string& str)
{
    return Buffer(str.begin(), str.end());
}

Buffer BufferUtils::fromArray(const unsigned char* buf, std::size_t size)
{
    Buffer data;
    for (std::size_t i = 0; i < size; ++i)
    {
        data.push_back(buf[i]);
    }

    return data;
}

std::string BufferUtils::toString(const Buffer& data)
{
    return std::string(data.begin(), data.end());
}

}

